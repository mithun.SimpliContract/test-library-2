import { TestBed } from '@angular/core/testing';

import { TestLibrary2Service } from './test-library2.service';

describe('TestLibrary2Service', () => {
  let service: TestLibrary2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestLibrary2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
