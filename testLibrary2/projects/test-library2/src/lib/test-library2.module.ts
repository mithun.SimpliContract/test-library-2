import { NgModule } from '@angular/core';
import { TestLibrary2Component } from './test-library2.component';



@NgModule({
  declarations: [
    TestLibrary2Component
  ],
  imports: [
  ],
  exports: [
    TestLibrary2Component
  ]
})
export class TestLibrary2Module { }
